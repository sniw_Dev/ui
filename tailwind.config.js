/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
 
    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/flowbite-react/**/*.js",

  ],
  theme: {
    fontFamily: {
      sans: ['Poppins', 'sans-serif']
    },

    extend: {
      colors: {
        ...colors,
        primarycolor:'#1e2434',
        secondarycolor:'#fd295d', 
        normalcolor:'#9b9395',
        specialcolor:'#757072',
      },
    },
  },


  plugins: [    require("flowbite/plugin")
],
}

// -webkit-linear-gradient(0deg, #fd295d, #fb865d 100%)