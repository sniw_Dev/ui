import { useState, useEffect } from 'react';
import jwtDecode from 'jwt-decode';

const useAuth = () => {
  const [authState, setAuthState] = useState({
    isAuthenticated: false,
    user: null,
  });

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedToken = jwtDecode(token);
      const currentTime = Date.now() / 1000;
      if (decodedToken.exp < currentTime) {
        localStorage.removeItem('token');
      } else {
        setAuthState({
          isAuthenticated: true,
          user: decodedToken.user,
        });
      }
    }
  }, []);

  const setAuthInfo = (token) => {
    localStorage.setItem('token', token);
    const decodedToken = jwtDecode(token);
    setAuthState({
      isAuthenticated: true,
      user: decodedToken.user,
    });
  };

  const logout = () => {
    localStorage.removeItem('token');
    setAuthState({
      isAuthenticated: false,
      user: null,
    });
  };

  return { authState, setAuthInfo, logout };
};

export default useAuth;
