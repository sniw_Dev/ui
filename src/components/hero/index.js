import React from 'react'
import styles from '@/styles/hero.module.css'
import dynamic from 'next/dynamic';

// import Map from '@/components/Maps/MapComponent'
const MapWithNoSSR = dynamic(() => import('@/components/Maps/MapComponent'), {
  ssr: false,
});

// const Hero = () => {
//   return (
//     <div className={styles.heroContainer}>
//     <div className={styles.heroContainerbackground}>
        
//     <h1 className={styles.title}>Simplifiez votre vie avec notre plateforme tout-en-un</h1>
//     <p className={styles.description}>Accédez à notre réseau de professionnels qualifiés d&apos;un simple clic. Dites adieu aux tracas de la gestion de plusieurs prestataires de services et simplifiez votre vie dès aujourd&apos;hui avec notre plateforme tout-en-un.</p>
//     <a href="/app/service" class="text-white mt-5 w-fit bg-primarycolor p-4 hover:p-5 hover:rotate-1 hover:scale-110 hover:transition-all hover:text-white  font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center">
//     Nos Services
//     <svg aria-hidden="true" class="w-5 h-5 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
// </a>
//     </div>
 
//     </div>
//   )
// }

const Hero = () => {
  return (
    // <div className={styles.heroContainer}>
    // <div className={styles.heroContainerbackground}>
        
    // </div>
 
    // </div>
 <div className={styles.heroContainer}>

    <MapWithNoSSR/>
    
     </div>


    )
}


export default Hero