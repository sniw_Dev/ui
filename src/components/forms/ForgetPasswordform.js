import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";

const ForgetPasswordform = () => {
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    const [isLoading, setisLoading] = useState(false);
    const [showSuccess,setshowSuccess] = useState(false); 
    const [showFail,setshowFail] = useState(false);
  
    const validationSchema = Yup.object({
        email: Yup.string().matches(emailRegex,'Please enter a valid email address').required("Required"),
    
        password: Yup.string().required("Required"),
      });
    
      const { handleChange, handleSubmit, values, errors } = useFormik({
        validationSchema: validationSchema,
        initialValues: {
          email: "",
        
        },
        validateOnChange: true,
        onSubmit: async (values) => {
          setisLoading(true);
          let isAuthenticated 
          console.log(isAuthenticated)
          if(isAuthenticated){
            console.log(isAuthenticated)
            setshowSuccess(true); 
            setTimeout(()=>{
                setshowSuccess(false); 
    
            },3000)
          }else{
            setshowFail(true); 
            setTimeout(()=>{
                setshowFail(false); 
    
            },3000)
            console.log(false)
            setisLoading(false);
    
          }
    
        },
      });
  return (
    <form className="space-y-5" onSubmit={handleSubmit}>
                
<div className="">
          <div class="relative mb-1">
            <div class="absolute inset-y-0 left-0 flex items-center focus:ring-0 pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-400 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
              value={values.email}
              onChange={handleChange}
              type="email"
              name="email"
              id="email"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.email ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="name@flowbite.com"
            />
          </div>
          {errors.email && (
            <span className="text-sm text-red-600">{errors.email}</span>
          )}
        </div>
            <button
          type="submit"
          disabled={Object.keys(errors).length === 0 ?false:true}
          className="w-full px-4 mb-3 py-3 text-white font-medium bg-primarycolor hover:bg-secondarycolor active:bg-indigo-600 rounded-lg duration-150"
        >Reset password </button>
    </form>
  )
}

export default ForgetPasswordform