import login from "@/apis/auth/login";
import { useFormik } from "formik";
import { useState } from "react";
import Link from "next/link";
import * as Yup from "yup";

const SignupForm = () => {
  const [isLoading, setisLoading] = useState(false);
  const [showSuccess, setshowSuccess] = useState(false);
  const [showFail, setshowFail] = useState(false);
  const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;

  const validationSchema = Yup.object({
    email: Yup.string()
      .matches(emailRegex, "Please enter a valid email address")
      .required("Required"),

    password: Yup.string().required("Required"),
  });

  const { handleChange, handleSubmit, values, errors } = useFormik({
    validationSchema: validationSchema,
    initialValues: {
      email: "",
      password: "",
      confirmPassword: "",
      fullName: "",
    },
    validateOnChange: true,
    onSubmit: async (values) => {
      setisLoading(true);
      let isAuthenticated = await login(values.email, values.password);
      console.log(isAuthenticated);
      if (isAuthenticated) {
        console.log(isAuthenticated);
        setshowSuccess(true);
        setTimeout(() => {
          setshowSuccess(false);
        }, 3000);
      } else {
        setshowFail(true);
        setTimeout(() => {
          setshowFail(false);
        }, 3000);
        console.log(false);
        setisLoading(false);
      }
    },
  });

  return (
    <>
      {showFail && (
        <div
          id="toast-top-right"
          class="fixed flex items-center w-full max-w-xs p-4 space-x-4 text-gray-500 bg-white divide-x divide-gray-200 rounded-lg shadow top-5 right-5 dark:text-gray-400 dark:divide-gray-700 space-x dark:bg-gray-800"
          role="alert"
        >
          <div class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-red-500 bg-red-100 rounded-lg dark:bg-red-800 dark:text-red-200">
            <svg
              aria-hidden="true"
              class="w-5 h-5"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clip-rule="evenodd"
              ></path>
            </svg>
            <span class="sr-only">Error icon</span>
          </div>
          <div class="ml-3 text-sm font-normal">Item has been deleted.</div>
        </div>
      )}
      {showSuccess && (
        <div
          id="toast-top-right"
          class="fixed flex items-center w-full max-w-xs p-4 space-x-4 text-gray-500 bg-white divide-x divide-gray-200 rounded-lg shadow top-5 right-5 dark:text-gray-400 dark:divide-gray-700 space-x dark:bg-gray-800"
          role="alert"
        >
          <div class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-green-500 bg-green-100 rounded-lg dark:bg-green-800 dark:text-green-200">
            <svg
              aria-hidden="true"
              class="w-5 h-5"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                clip-rule="evenodd"
              ></path>
            </svg>
            <span class="sr-only">Check icon</span>
          </div>
          <div class="ml-3 text-sm font-normal">
            Connexion réussie ! , redirection...
          </div>
        </div>
      )}
      <form className="px-5" onSubmit={handleSubmit}>
        <div className="py-2">
          <label className="font-normal mb-1 text-specialcolor ">
            Full name{" "}
          </label>
          <div class="relative ">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                class="text-gray-400 w-5 h-5"
              >
                <path
                  fill-rule="evenodd"
                  d="M7.5 6a4.5 4.5 0 119 0 4.5 4.5 0 01-9 0zM3.751 20.105a8.25 8.25 0 0116.498 0 .75.75 0 01-.437.695A18.683 18.683 0 0112 22.5c-2.786 0-5.433-.608-7.812-1.7a.75.75 0 01-.437-.695z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
            <input
              value={values.fullName}
              onChange={handleChange}
              type="text"
              name="fullName"
              id="fullName"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.fullName ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="john anderson"
            />
          </div>
          {errors.fullName && (
            <span className="text-sm text-red-600">{errors.fullName}</span>
          )}
        </div>

        <div className="py-2">
          <label className="font-normal mb-1 text-specialcolor">Email</label>
          <div class="relative mb-1">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-400 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
              value={values.email}
              onChange={handleChange}
              type="email"
              name="email"
              id="email"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.email ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="name@flowbite.com"
            />
          </div>
          {errors.email && (
            <span className="text-sm text-red-600">{errors.email}</span>
          )}
        </div>
        <div className="py-2">
          <label className="font-normal mb-1 text-specialcolor ">
            Password
          </label>
          <div class="relative mb-1">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                class="w-5 h-5 text-gray-400"
              >
                <path
                  fill-rule="evenodd"
                  d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
            <input
              value={values.password}
              onChange={handleChange}
              type="password"
              name="password"
              id="password"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.password ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="***********"
            />
          </div>
          {errors.password && (
            <span className="text-sm text-red-600">{errors.password}</span>
          )}
        </div>
        <div className="py-2">
          <label className="font-normal mb-1 text-specialcolor ">
            Confirm password
          </label>
          <div class="relative mb-1">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                class="w-5 h-5 text-gray-400"
              >
                <path
                  fill-rule="evenodd"
                  d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
            <input
              value={values.confirmPassword}
              onChange={handleChange}
              type="password"
              name="confirmPassword"
              id="confirmPassword"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.email ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="***********"
            />
          </div>
          {errors.confirmPassword && (
            <span className="text-sm text-red-600">
              {errors.confirmPassword}
            </span>
          )}
        </div>
        <div className="flex py-2 items-center justify-between text-sm">
          <div className="flex items-center gap-x-3">
            <label class="relative inline-flex items-center mb-2 cursor-pointer">
              <input type="checkbox" value="" class="sr-only peer" />
              <div class="w-9 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-0 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-secondarycolor"></div>
              <span class="ml-3 text-sm font-medium text-normalcolor dark:text-gray-300">
                I agree to the terms of services{" "}
              </span>
            </label>
          </div>
        </div>
        <button
          type="submit"
          disabled={Object.keys(errors).length === 0 ? false : true}
          className="w-full px-4 py-4 inline-flex items-center justify-center text-center text-white font-medium bg-primarycolor hover:bg-secondarycolor active:bg-indigo-600 rounded-lg duration-150"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-5 h-5 mr-2 -ml-1"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
            />
          </svg>

          {isLoading ? (
            <div role="status">
              <svg
                aria-hidden="true"
                class="inline w-8 h-8 mr-2 text-gray-400 animate-spin dark:text-gray-600 fill-white dark:fill-gray-300"
                viewBox="0 0 100 101"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                  fill="currentColor"
                />
                <path
                  d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                  fill="currentFill"
                />
              </svg>
              <span class="sr-only">Loading...</span>
            </div>
          ) : (
            `Create account`
          )}
        </button>
        <div className="mt-4 font-light text-center">
          Already have an account ?
          <Link 
            href="/auth"
            className="text-secondarycolor font-bold cursor-pointer hover:text-primarycolor"
          >{` SIGN IN`}</Link>
        </div>
      </form>
    </>
  );
};

export default SignupForm;
