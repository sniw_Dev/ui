import React from 'react'
import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";

const ResetPasswordform = () => {
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    const [isLoading, setisLoading] = useState(false);
    const [showSuccess,setshowSuccess] = useState(false); 
    const [showFail,setshowFail] = useState(false);
  
    const validationSchema = Yup.object({
    
        password: Yup.string().required("Required"),

      });
    
      const { handleChange, handleSubmit, values, errors } = useFormik({
        validationSchema: validationSchema,
        initialValues: {
          password: "",
          confirmPassword: "",
        },
        validateOnChange: true,
        onSubmit: async (values) => {
          setisLoading(true);
          let isAuthenticated 
          console.log(isAuthenticated)
          if(isAuthenticated){
            console.log(isAuthenticated)
            setshowSuccess(true); 
            setTimeout(()=>{
                setshowSuccess(false); 
    
            },3000)
          }else{
            setshowFail(true); 
            setTimeout(()=>{
                setshowFail(false); 
    
            },3000)
            console.log(false)
            setisLoading(false);
    
          }
    
        },
      });
  return (
    <form className="space-y-5" onSubmit={handleSubmit}>
                
<div className="">
          <div class="relative mb-4">
            <div class="absolute inset-y-0 left-0 flex items-center focus:ring-0 pl-3 pointer-events-none">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 text-gray-300">
  <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z" />
</svg>

            </div>
            <input
              value={values.password}
              onChange={handleChange}
              type="password"
              name="password"
              id="password"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.password ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="Password"
            />
          </div>
          {errors.password && (
            <span className="text-sm text-red-600">{errors.password}</span>
          )}

<div class="relative mb-1">
            <div class="absolute inset-y-0 left-0 flex items-center focus:ring-0 pl-3 pointer-events-none">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 text-gray-300">
  <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z" />
</svg>

            </div>
            <input
              value={values.password}
              onChange={handleChange}
              type="password"
              name="password"
              id="password"
              className={`bg-white h-12 text-gray-900 text-sm rounded-lg focus:ring-pink-400 border-gray-300 block w-full pl-10 p-2.5   ${
                errors.password ? "border-red-600 " : "border-gray-300"
              }   `}
              placeholder="Confirm password"
            />
          </div>
          {errors.password && (
            <span className="text-sm text-red-600">{errors.password}</span>
          )}
        </div>
            <button
          type="submit"
          disabled={Object.keys(errors).length === 0 ?false:true}
          className="w-full px-4 mb-3 py-3 text-white font-medium bg-primarycolor hover:bg-secondarycolor active:bg-indigo-600 rounded-lg duration-150"
        >Update password </button>
    </form>
  )
}

export default ResetPasswordform