import axiosInstance from '../../lib/axios';
import jwt from 'jsonwebtoken';
import Cookies from 'js-cookie';

export default async function login(useremail, password) {
  try {
    const response = await axiosInstance.post('/auth/signin', {
      email: useremail,
      password: password,
    });
    const {
        role,
        _id,
        first_name,
        last_name, 
        email,
        token
    } = response.data
    

    // Save the JWT token to a cookie
    Cookies.set('token', token);
    Cookies.set('role', role);
    Cookies.set('_id', _id);
    Cookies.set('first_name', first_name);
    Cookies.set('last_name', last_name);
    Cookies.set('email', email);
    // Decode the JWT token to get the user information
    const decoded = jwt.decode(token);
    console.log(decoded)

    return true;
  } catch (error) {
    console.error(error);

    return false;

  }
}