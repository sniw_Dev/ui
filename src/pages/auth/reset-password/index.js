import ResetPasswordform from '@/components/forms/ResetPasswordform'
import styles from '@/styles/resetpassword.module.css'
import Link from 'next/link'

const ResetPassword = () => {
  return (
    <main className={styles.main}>
    <div style={{minWidth:'32rem'}}className="max-w-sm bg-white p-6 rounded-md text-gray-600 space-y-5">

        
        <div className="text-center text-3xl py-2 font-medium flex items-center justify-evenly">
    

          <span>Create new password</span> 
          </div>
        <div className='text-md text-center mt-2 text-gray-500'>
        Please fill out the fields below to change your password.
        </div>
        <ResetPasswordform/>

    </div>
</main>   )
}

export default ResetPassword