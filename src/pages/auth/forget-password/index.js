import styles from '@/styles/forgetpassword.module.css'
import ForgetPasswordform from '@/components/forms/ForgetPasswordform'
import Link from 'next/link'
const ForgetPassword = () => {
  return (
    <main className={styles.main}>
    <div style={{minWidth:'32rem'}}className="max-w-sm bg-white p-6 rounded-md text-gray-600 space-y-5">
        <Link href='/auth'>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-9 h-9 hover:text-secondarycolor">
  <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18" />
</svg>
        </Link>
        
        <div className="text-center text-3xl py-2 font-medium flex items-center justify-evenly">
    

          <span>Forgot your password?</span> 
          </div>
        <div className='text-md text-center mt-2 text-gray-500'>
        Please enter your email address below and we&apos;ll send you a link to reset your password. 

        </div>
        <ForgetPasswordform/>

    </div>
</main>  )
}

export default ForgetPassword