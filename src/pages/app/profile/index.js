import React from "react";
import Applayout from "@/layout/Applayout";
import { useRouter } from "next/router";
import animations from "@/styles/animations.module.css";
const index = () => {
  const ProfileAvatar = () => {
    return (
      <div className=" flex items-end z-0  relative top-20  left-10 w-full">
        <img
          class="w-36 h-36 p-1 rounded-full  ring-4 ring-secondarycolor  dark:ring-gray-500"
          src="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
          alt="Bordered avatar"
        ></img>
        <div className="pb-0 relative mr-10 flex w-full justify-between p-7 items-center">
          <div>
            <div className="text-2xl text-primarycolor  font-medium">
              Jhon Anderson
            </div>
            <div>
              Co-founder @Startup, 2021 Forbes 30 Under 30 in Life & Style
            </div>
          </div>
          <div className="mr-10 flex items-center">
            <button
              type="button"
              class="text-white bg-primarycolor hover:transition-all hover:scale-110 hover:bg-secondarycolor/90 focus:ring-4 focus:outline-none focus:ring-[#3b5998]/50 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-[#3b5998]/55 mr-2 mb-2"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-4 h-4 mr-2 -ml-1"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M7.5 8.25h9m-9 3H12m-9.75 1.51c0 1.6 1.123 2.994 2.707 3.227 1.129.166 2.27.293 3.423.379.35.026.67.21.865.501L12 21l2.755-4.133a1.14 1.14 0 01.865-.501 48.172 48.172 0 003.423-.379c1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z"
                />
              </svg>
              Message
            </button>
          </div>
        </div>
      </div>
    );
  };

  const HeaderContent = () => {
    return (
      <div className="px-8 pb-6 pt-20 flex justify-between items-center">
        <div className="w-1/2">
          <div className="p-2 text-lg text-primarycolor font-medium">About</div>
          <div className="p-1 text-specialcolor">
            Transitioning into the wonderful world of technology, I am thrilled
            to embark on this new journey as a Software Engineer. Stepping into
            this domain with an inquisitive mindset as I conjoin my decade-plus
            experiences from fashion, beauty and production industries.
          </div>
        </div>
        <div className="w-1/2 flex items-end flex-col justify-start ">
          <div>
            <div className="p-2 ml-7 text-lg text-primarycolor font-medium">
              Education
            </div>

            <ul>
              <li className="p-1  ">
                <div className="flex gap-x-3 items-center">
                  <img
                    src="/assets/icons/scholarship.png"
                    alt=""
                    className="h-8 w-8"
                  />
                  <div className="text-specialcolor font-medium">
                    {" "}
                    Texas Southern University
                  </div>
                </div>
              </li>
              <li className="p-1  ">
                <div className="flex gap-x-3">
                  <img
                    src="/assets/icons/scholarship.png"
                    alt=""
                    className="h-8 w-8"
                  />

                  <div className="font-medium text-specialcolor">
                    {" "}
                    University of Niagara at Urbana Champaign
                  </div>
                </div>
              </li>
              <li className="p-1  ">
                <div className="flex gap-x-3">
                  <img
                    src="/assets/icons/scholarship.png"
                    alt=""
                    className="h-8 w-8"
                  />

                  <div className="font-medium text-specialcolor">
                    {" "}
                    Maine Township Hich School 207D
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  };
  const ProfileHeader = () => {
    return (
      <div className={` max-w-full break-inside bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700`}>
        <div>
          <div
            style={{
              position: "relative",
              backgroundImage: `url(https://images.ctfassets.net/pdf29us7flmy/1R87JzhyBK9XhLMeA6gSKY/a3fb3a0bf7ad3a3691369a5edb633952/Start-cover-letter_social2.png)`,
              // background:' -webkit-linear-gradient(0deg, #fd295d, #fb865d 100%);',
              backgroundPosition: "center",
              objectFit: "contain",
              backgroundRepeat: "no-repeat",
            }}
            class="rounded-lg max-w-full h-36 bg-primarycolor"
          >
            <ProfileAvatar />
          </div>

          <HeaderContent />
        </div>
      </div>
    );
  };

  const ServiceCard = () => {
    const router = useRouter();

    return (
      <article class="mb-0 break-inside p-4 hover:scale-110  transition-all rounded-xl shadow-2xl bg-white dark:bg-slate-800 flex flex-col bg-clip-border">
        <div class="flex pb-1 items-center justify-between"></div>
        <h2 class="text-lg  font-extrabold">Web Design templates Selection</h2>
        <div class="flex items-center">
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>First star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Second star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Third star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Fourth star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-gray-300 dark:text-gray-500"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Fifth star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
        </div>
        <div class="py-1 font-normal text-specialcolor">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
        </div>
        <button
          onClick={() => router.push("/app/profile")}
          type="submit"
          class="text-white mr-2 bg-primarycolor hover:transition-all hover:scale-110 hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Consulter
        </button>
      </article>
    );
  };

  const ServicesList = () => {
    return (
      <div
        style={{
          position: "relative",
          backgroundImage: `url(https://images.ctfassets.net/pdf29us7flmy/1R87JzhyBK9XhLMeA6gSKY/a3fb3a0bf7ad3a3691369a5edb633952/Start-cover-letter_social2.png)`,
          // background:' -webkit-linear-gradient(0deg, #fd295d, #fb865d 100%);',
          backgroundPosition: "center",
          objectFit: "contain",
          backgroundRepeat: "no-repeat",
        }}
        className="max-w-full break-inside  bg-primarycolor/50 bg-blend-multiply py-2 px-4 h-4/5  border border-gray-200 rounded-lg shadow"
      >
        <div class="grid  mt-8 grid-cols-3 gap-6 text-white text-2xl font-medium">
          Services
        </div>
        <div class="grid  mt-8 grid-cols-3 gap-6">
          <ServiceCard />
          <ServiceCard />
          <ServiceCard />
        </div>
      </div>
    );
  };
  const SimilarProfileCard = () => {
    const router = useRouter();

    return (
      <article class="mb-0 break-inside p-4 hover:scale-110  transition-all rounded-xl shadow-2xl bg-white dark:bg-slate-800 flex flex-col bg-clip-border">
        <div class="flex pb-1 items-center justify-between"></div>
        <div  style={{
          position: "relative",
          backgroundImage: `url(https://images.ctfassets.net/pdf29us7flmy/1R87JzhyBK9XhLMeA6gSKY/a3fb3a0bf7ad3a3691369a5edb633952/Start-cover-letter_social2.png)`,
          // background:' -webkit-linear-gradient(0deg, #fd295d, #fb865d 100%);',
          backgroundPosition: "center",
          objectFit: "contain",
          backgroundRepeat: "no-repeat",
        }}class="flex  p-3 gap-x-3 rounded-lg items-center justify-Start ">
        <div>
        <img
          class="w-20 h-20 p-1 rounded-full  ring-2 ring-white  dark:ring-gray-500"
          src="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
          alt="Bordered avatar"
        ></img>
        </div>
        <div>
        <h2 class="text-lg text-white font-extrabold text-left">Jhon Anderson</h2>
        <div class="flex items-center justify-start">
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>First star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Second star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Third star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Fourth star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-gray-300 dark:text-gray-500"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Fifth star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
        </div>
        </div>
        </div>
        
        <div class="py-1 pt-3 font-normal text-specialcolor text-left">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
        </div>
        <button
          onClick={() => router.push("/app/profile")}
          type="submit"
          class="text-white mr-2 bg-primarycolor hover:transition-all hover:scale-110 hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          View profile
        </button>
      </article>
    );
  };
  const SimilarServicesList = () => {
    return (
      <div
    
        className={` max-w-full    bg-primarycolor py-2 px-4 h-4/5  border border-gray-200 rounded-lg shadow`}
      >
        <div class="grid  mt-8 grid-cols-3 gap-6 text-white text-2xl font-medium">
          Similar profiles
        </div>
        <div class="grid  mt-8 grid-cols-3 gap-6">
          <SimilarProfileCard />
          <SimilarProfileCard />
          <SimilarProfileCard />
        </div>
      </div>
    );
  };

  const MenuRealisation = () => {
    return (
      <div   style={{
        position: "relative",
        backgroundImage: `url(https://images.ctfassets.net/pdf29us7flmy/1R87JzhyBK9XhLMeA6gSKY/a3fb3a0bf7ad3a3691369a5edb633952/Start-cover-letter_social2.png)`,
        // background:' -webkit-linear-gradient(0deg, #fd295d, #fb865d 100%);',
        backgroundPosition: "center",
        objectFit: "contain",
        backgroundRepeat: "no-repeat",
      }} class="w-full col-span-1 max-w-sm p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-6 dark:bg-gray-800 dark:border-gray-700">
        <h5 class="mb-3 text-base font-semibold text-white md:text-xl dark:text-white">
          Realisations
        </h5>
        <p class="text-sm font-normal text-gray-500 dark:text-gray-400"></p>
        <ul class="pl-0 space-y-3">
          <li>
            <a
              href="#"
              class="flex hover:scale-105  transition-all items-center p-3 text-base font-bold text-primarycolor rounded-lg bg-gray-50 hover:bg-secondarycolor   hover:text-white group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white"
            >
              <span class="flex-1 ml-3 whitespace-nowrap">MetaMask</span>
              <span class="inline-flex items-center justify-center px-2 py-0.5 ml-3 text-xs font-medium text-gray-500 bg-gray-200 rounded dark:bg-gray-700 dark:text-gray-400">
                Popular
              </span>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex hover:scale-105  transition-all items-center p-3 text-base font-bold text-primarycolor rounded-lg bg-gray-50 hover:bg-secondarycolor   hover:text-white group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white"
            >
              {/* <svg
                aria-hidden="true"
                class="h-5"
                viewBox="0 0 292 292"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M145.7 291.66C226.146 291.66 291.36 226.446 291.36 146C291.36 65.5541 226.146 0.339844 145.7 0.339844C65.2542 0.339844 0.0400391 65.5541 0.0400391 146C0.0400391 226.446 65.2542 291.66 145.7 291.66Z"
                  fill="#3259A5"
                />
                <path
                  d="M195.94 155.5C191.49 179.08 170.8 196.91 145.93 196.91C117.81 196.91 95.0204 174.12 95.0204 146C95.0204 117.88 117.81 95.0897 145.93 95.0897C170.8 95.0897 191.49 112.93 195.94 136.5H247.31C242.52 84.7197 198.96 44.1797 145.93 44.1797C89.6904 44.1797 44.1104 89.7697 44.1104 146C44.1104 202.24 89.7004 247.82 145.93 247.82C198.96 247.82 242.52 207.28 247.31 155.5H195.94Z"
                  fill="white"
                />
              </svg> */}
              <span class="flex-1 ml-3 whitespace-nowrap">Coinbase Wallet</span>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center scale-105  transition-all p-3 text-base font-bold text-primarycolor rounded-lg  bg-secondarycolor   text-white group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white"
            >
              {/* <svg
                aria-hidden="true"
                svg
                class="h-5"
                viewBox="0 0 75.591 75.591"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
              >
                <linearGradient
                  id="a"
                  gradientTransform="matrix(0 -54.944 -54.944 0 23.62 79.474)"
                  gradientUnits="userSpaceOnUse"
                  x2="1"
                >
                  <stop offset="0" stop-color="#ff1b2d" />
                  <stop offset=".3" stop-color="#ff1b2d" />
                  <stop offset=".614" stop-color="#ff1b2d" />
                  <stop offset="1" stop-color="#a70014" />
                </linearGradient>
                <linearGradient
                  id="b"
                  gradientTransform="matrix(0 -48.595 -48.595 0 37.854 76.235)"
                  gradientUnits="userSpaceOnUse"
                  x2="1"
                >
                  <stop offset="0" stop-color="#9c0000" />
                  <stop offset=".7" stop-color="#ff4b4b" />
                  <stop offset="1" stop-color="#ff4b4b" />
                </linearGradient>
                <g transform="matrix(1.3333 0 0 -1.3333 0 107.2)">
                  <path
                    d="m28.346 80.398c-15.655 0-28.346-12.691-28.346-28.346 0-15.202 11.968-27.609 26.996-28.313.44848-.02115.89766-.03314 1.3504-.03314 7.2574 0 13.876 2.7289 18.891 7.2137-3.3227-2.2036-7.2074-3.4715-11.359-3.4715-6.7504 0-12.796 3.3488-16.862 8.6297-3.1344 3.6999-5.1645 9.1691-5.3028 15.307v1.3349c.13821 6.1377 2.1683 11.608 5.302 15.307 4.0666 5.2809 10.112 8.6297 16.862 8.6297 4.1526 0 8.038-1.2679 11.361-3.4729-4.9904 4.4643-11.569 7.1876-18.786 7.2144-.03596 0-.07122.0014-.10718.0014z"
                    fill="url(#a)"
                  />
                  <path
                    d="m19.016 68.025c2.6013 3.0709 5.9607 4.9227 9.631 4.9227 8.2524 0 14.941-9.356 14.941-20.897s-6.6891-20.897-14.941-20.897c-3.6703 0-7.0297 1.851-9.6303 4.922 4.0659-5.2809 10.111-8.6297 16.862-8.6297 4.1519 0 8.0366 1.2679 11.359 3.4715 5.802 5.1906 9.4554 12.735 9.4554 21.133 0 8.397-3.6527 15.941-9.4533 21.131-3.3234 2.205-7.2088 3.4729-11.361 3.4729-6.7504 0-12.796-3.3488-16.862-8.6297"
                    fill="url(#b)"
                  />
                </g>
              </svg> */}
              <span class="flex-1 ml-3 whitespace-nowrap">Opera Wallet</span>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center hover:scale-105  transition-all p-3 text-base font-bold text-primarycolor rounded-lg bg-gray-50 hover:bg-secondarycolor   hover:text-white group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white"
            >
              {/* <svg
                aria-hidden="true"
                class="h-5"
                viewBox="0 0 512 512"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
              >
                <defs>
                  <radialGradient
                    cx="0%"
                    cy="50%"
                    fx="0%"
                    fy="50%"
                    r="100%"
                    id="radialGradient-1"
                  >
                    <stop stop-color="#5D9DF6" offset="0%"></stop>
                    <stop stop-color="#006FFF" offset="100%"></stop>
                  </radialGradient>
                </defs>
                <g
                  id="Page-1"
                  stroke="none"
                  stroke-width="1"
                  fill="none"
                  fill-rule="evenodd"
                >
                  <g id="logo">
                    <rect
                      id="base"
                      fill="url(#radialGradient-1)"
                      x="0"
                      y="0"
                      width="512"
                      height="512"
                      rx="256"
                    ></rect>
                    <path
                      d="M169.209772,184.531136 C217.142772,137.600733 294.857519,137.600733 342.790517,184.531136 L348.559331,190.179285 C350.955981,192.525805 350.955981,196.330266 348.559331,198.676787 L328.82537,217.99798 C327.627045,219.171241 325.684176,219.171241 324.485851,217.99798 L316.547278,210.225455 C283.10802,177.485633 228.89227,177.485633 195.453011,210.225455 L186.951456,218.549188 C185.75313,219.722448 183.810261,219.722448 182.611937,218.549188 L162.877976,199.227995 C160.481326,196.881474 160.481326,193.077013 162.877976,190.730493 L169.209772,184.531136 Z M383.602212,224.489406 L401.165475,241.685365 C403.562113,244.031874 403.562127,247.836312 401.165506,250.182837 L321.971538,327.721548 C319.574905,330.068086 315.689168,330.068112 313.292501,327.721609 C313.292491,327.721599 313.29248,327.721588 313.29247,327.721578 L257.08541,272.690097 C256.486248,272.103467 255.514813,272.103467 254.915651,272.690097 C254.915647,272.690101 254.915644,272.690105 254.91564,272.690108 L198.709777,327.721548 C196.313151,330.068092 192.427413,330.068131 190.030739,327.721634 C190.030725,327.72162 190.03071,327.721606 190.030695,327.721591 L110.834524,250.181849 C108.437875,247.835329 108.437875,244.030868 110.834524,241.684348 L128.397819,224.488418 C130.794468,222.141898 134.680206,222.141898 137.076856,224.488418 L193.284734,279.520668 C193.883897,280.107298 194.85533,280.107298 195.454493,279.520668 C195.454502,279.520659 195.45451,279.520651 195.454519,279.520644 L251.65958,224.488418 C254.056175,222.141844 257.941913,222.141756 260.338618,224.488222 C260.338651,224.488255 260.338684,224.488288 260.338717,224.488321 L316.546521,279.520644 C317.145683,280.107273 318.117118,280.107273 318.71628,279.520644 L374.923175,224.489406 C377.319825,222.142885 381.205562,222.142885 383.602212,224.489406 Z"
                      id="WalletConnect"
                      fill="#FFFFFF"
                      fill-rule="nonzero"
                    ></path>
                  </g>
                </g>
              </svg> */}
              <span class="flex-1 ml-3 whitespace-nowrap">WalletConnect</span>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center hover:scale-105  transition-all p-3 text-base font-bold text-primarycolor rounded-lg bg-gray-50 hover:bg-secondarycolor   hover:text-white group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white"
            >
              {/* <svg
                aria-hidden="true"
                class="h-4"
                viewBox="0 0 96 96"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M72.0998 0.600098H48.3998H24.5998H0.799805V24.4001V48.2001V49.7001V71.8001V71.9001V95.5001H24.5998V72.0001V71.9001V49.8001V48.3001V24.5001H48.3998H72.1998H95.9998V0.700104H72.0998V0.600098Z"
                  fill="#617BFF"
                />
                <path
                  d="M48.5 71.8002H72.1V95.6002H73C79.1 95.6002 84.9 93.2002 89.2 88.9002C93.5 84.6002 95.9 78.8002 95.9 72.7002V48.2002H48.5V71.8002Z"
                  fill="#617BFF"
                />
              </svg> */}
              <span class="flex-1 ml-3 whitespace-nowrap">Fortmatic</span>
            </a>
          </li>
        </ul>
        <div></div>
      </div>
    );
  };
  const DescriptionCard = () => {
    const router = useRouter();

    return (
      <article class="mb-0 h-full break-inside p-4 hover:scale-105  transition-all rounded-xl shadow-2xl bg-white dark:bg-slate-800 flex flex-col bg-clip-border">
        <div class="flex pb-1 items-center justify-between"></div>
        <h2 class="text-lg  font-extrabold">Opera Wallet</h2>
        <div class="flex items-center">
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>First star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Second star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Third star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-yellow-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Fourth star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
          <svg
            aria-hidden="true"
            class="w-5 h-5 text-gray-300 dark:text-gray-500"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Fifth star</title>
            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
          </svg>
        </div>
        <div class="py-1 font-normal text-specialcolor">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doLorem
            ipsum dolor sit amet, consectetur adipiscing elit, sed doLorem ipsum
            dolor sit amet, consectetur adipiscing elit, sed doLorem ipsum dolor
            sit amet, consectetur adipiscing elit, sed doLorem ipsum dolor sit
            amet, consectetur adipiscing elit, sed doLorem ipsum dolor sit amet,
            sit amet, consectetur adipiscing elit, sed doLorem ipsum dolor sit
            amet, consectetur adipiscing elit, sed doLorem ipsum dolor sit amet
          </p>
        </div>
        <button
          onClick={() => router.push("/app/profile")}
          type="submit"
          class="text-white mr-2 bg-primarycolor hover:transition-all hover:scale-110 hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          See feedback
        </button>
      </article>
    );
  };

  const GaleryRealisations = () => {
    return (
      <div class="grid gap-4 col-span-2 grid-cols-1 relative  bg-white shadow p-4 rounded-lg">
        <div class="grid gap-4 ">
          <div className="grid gap-4 grid-cols-2">
            {/* <div className="col-span-2">
        <img class="h-56 max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/featured/image.jpg" alt=""/>
        </div> */}
            <div>
              <DescriptionCard />
            </div>
            <div class="grid grid-cols-3 gap-4 bg-second p-4 rounded-lg">
              <div className="col-span-3">
                <img
                  class="h-56 w-full hover:scale-110 transition-all max-w-full rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThywHmzw_8YTfUPvz5W-JMFeAaokMFizuAAkb6m13gBZf-T2Tvn0-EdZKKDY3PQI3h3M4&usqp=CAU"

                  alt=""
                />
              </div>
              <div>
                <img
                  class="h-20 max-w-full max-w-full hover:scale-150 transition-all rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQc6Yal75rS02yiB5pshOuEQeM9ldrAYB8mJg&usqp=CAU"
                  alt=""
                />
              </div>
              <div>
                <img
                  class="h-20 max-w-full max-w-full hover:scale-150 transition-all rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7i0HPX77nh_QzuwknPzNtYnM26U7v4N7xwVQk3ES0Cvoo502wBX7SgSnRrZLTUiXWBug&usqp=CAU"
                  alt=""
                />
              </div>
              <div>
                <img
                  class="h-20 max-w-full max-w-full hover:scale-150 transition-all rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8F5NhSrBFltYBK9zLH5FrDOIw1lRku-7ONuvh0fd1Yvyo1iZoGeu3RjwBj5mczEzsPCk&usqp=CAU"
                  alt=""
                />
              </div>
              <div>
                <img
                  class="h-20 max-w-full max-w-full hover:scale-150 transition-all rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPAd7cu5631Nxchv2CgdsBBqqjVosrRqUyDQ&usqp=CAU"
                  alt=""
                />
              </div>
              <div>
                <img
                  class="h-20 max-w-full max-w-full hover:scale-150 transition-all rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRK4pdov0siFagIS8_2Q-JEEs99jwqNGCDeEw&usqp=CAU"
                  alt=""
                />
              </div>
              <div>
                <img
                  class="h-20 max-w-full max-w-full hover:scale-150 transition-all rounded-lg"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTowhENwkn7paV0XF4e2UsmvLWMcog5lCRg3r_hAGQfSx8Phg14MPAo95uZopaIfCkWLJY&usqp=CAU"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const RealisationCard = () => {
    return (
      <div class="grid   grid-cols-3 ">
        <MenuRealisation />
        <GaleryRealisations />
      </div>
    );
  };

  return (
    <div style={{ minHeight: "100vh" }} className={`${animations.animatesildein} h-full`}>
      <div class="grid  mt-8 grid-cols-1 gap-2">
        <ProfileHeader />
        <ServicesList />
        <RealisationCard />
        <SimilarServicesList/>
      </div>
    </div>
  );
};
index.Layout = Applayout;
export default index;
