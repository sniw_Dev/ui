import React from "react";
import dynamic from "next/dynamic";
import Applayout from "@/layout/Applayout";

const ApexChart = dynamic(() => import("react-apexcharts"), { ssr: false });
const ColumnChart = () => {
  const series = [
    {
      name: "Somme de Consommation ",
      data: [0.11, 0.04, 0.04, 0.09, 0.01],
    },
    {
      name: "Somme de Objectif",
      data: [0.06, 0.05, 0.01, 0.04, 0.02],
    },
  ];
  const options = {
    chart: {
      type: "bar",
      height: 250,
      stacked: true,
      toolbar: {
        show: true,
      },
      
      zoom: {
        enabled: true,
      },
    },

    title: {
      text: "Taux de consommation des piéces de rechangepar rapport a l'objectif",
      align: "center",
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          legend: {
            position: "bottom",
            offsetX: -10,
            offsetY: 0,
          },
        },
      },
    ],
    plotOptions: {
      bar: {
        horizontal: false,
        borderRadius: 10,
      },
    },
    yaxis: {
      title: {
        text: "Somme de Nombre des interventiones",
      },
    },

    xaxis: {
      categories: [
        "Coupe",
        "Contrôle électrique",
        "Outils de sertissage",
        "Assemblage",
        "Préparation",
      ],
      title: {
        
      },
    },
    legend: {
      position: "right",
      offsetY: 40,
    },
    fill: {
      opacity: 1,
    },
  };

  return (
    <div>
      <ApexChart options={options} series={series} type="bar" height={250} />
    </div>
  );
};

const PieChart = () => {
  const series = [
    21850.5937093333, 59010.6455834285, 20855.8836893333, 49485.0985729334,
    6435.249974,
  ];
  const options = {
    chart: {
      type: "donut",
    },
    labels: [
      "Contrôle électrique",
      "Coupe",
      "Outils de sertissage",
      "Assemblage",
      "Préparation",
    ],
    title: {
      text: "Taux de consommation des piéces de rechange par atelier",
      align: "center",
    },
    dataLabels: {
      enabled: true,
      formatter: function (val) {
        return parseFloat(val).toFixed(2) + "%";
      },
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div id="chart">
      <ApexChart options={options} series={series} type="donut" height={200} />
    </div>
  );
};
const CombinedCharts = () => {
  const series = [
    {
      name: "Somme de Poste2",
      type: "column",
      data: [
        407, 482, 359, 187, 198, 344, 176, 73, 185, 184, 101, 357, 110, 105,
        189, 66, 0, 436, 432, 47, 97, 78, 434, 292, 107, 173, 78, 86, 9, 85,
        102, 82, 183, 93, 84, 163, 152, 98, 153,
      ],
    },
    {
      name: "Somme de Poste1",
      type: "column",
      data: [
        459, 651, 244, 178, 164, 350, 0, 284, 0, 153, 148, 407, 7, 188, 246,
        199, 187, 368, 553, 110, 162, 96, 689, 268, 82, 202, 17, 86, 53, 52,
        219, 80, 223, 182, 19, 281, 0, 0, 206,
      ],
    },

    {
      name: "Somme de Poste3",
      type: "column",
      data: [
        4, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 117, 0, 0, 0, 0, 168, 0, 0, 74, 0,
        0, 113, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      ],
    },
    {
      name: "Total général",
      type: "line",
      data: [
        870, 1133, 603, 365, 372, 694, 176, 357, 185, 337, 249, 866, 117, 293,
        435, 265, 187, 168, 985, 157, 333, 174, 1123, 673, 189, 442, 95, 172,
        62, 137, 321, 162, 361, 275, 103, 444, 152, 98, 359,
      ],
    },
  ];
  const options = {
    chart: {
      height: 350,
      type: "line",
      stacked: true,
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: [1, 1, 4],
    },
    title: {
      text: "Somme des arrets par machine",
      align: "center",

    },
    xaxis: {
      categories: [
        "TCE VW 11",
        "TCE PSA 3 CB+",
        "TCE VW 15",
        "TCE 52",
        "TCE VW 42",
        "TCE VW 46",
        "TCE VW 13",
        "TCE VW 43",
        "TCE VW 14",
        "TCE VW 17",
        "TCE VW 23",
        "TCE WABCO 2",
        "TCE VW 39",
        "TCE VW 37",
        "TCE 55",
        "TCE VW 19",
        "TCE VW 41",
        "TCE WABCO 1",
        "TCE 51",
        "TCE VW 21",
        "TCE VW 26",
        "TCE VW 20",
        "TCE 53",
        "TCE VW 31",
        "TCE VW 50",
        "TCE VW 16",
        "TCE VW 29",
        "TCE VW 18",
        "TCE VW 34",
        "TCE VW 47",
        "TCE VW 32",
        "TCE VW 33",
        "TCE VW 45",
        "TCE VW 24",
        "TCE 56",
        "TCE VW 44",
        "TCE Batterie EC5",
        "TCE Moteur EC5",
        "TCE VW 22",
        "TCE VW 49",
      ],
    },
    yaxis: [
      {
        axisTicks: {
          show: true,
        },
        axisBorder: {
          show: true,
          color: "#008FFB",
        },
        labels: {
          style: {
            colors: "#008FFB",
          },
        },
    }
    ],
    tooltip: {
      fixed: {
        enabled: true,
        position: "topLeft", // topRight, topLeft, bottomRight, bottomLeft
        offsetY: 30,
        offsetX: 60,
      },
    },
    legend: {
      horizontalAlign: "left",
      offsetX: 40,
    },
  };

  return (
    <div >
      <ApexChart options={options} series={series} type="line" height={350} />
    </div>
  );
};
const CombinedChart = () => {
  const series = [
    {
      name: "Somme de Poste2",
      type: "column",
      data: [6224, 11269, 10318, 2533,4546],
    },
    {
      name: "Somme de Poste1",
      type: "column",
      data: [3875, 9667, 3081, 1629,2370],

    },

    {
      name: "Somme de Poste3",
      type: "column",
      data: [1197, 928, 2670, 1377,2454],

    },
    {
      name: "Total Duree",
      type: "line",
      data: [11296, 21864, 16069, 5539,9370],

    },
  ];
  const options = {
    chart: {
      height: 350,
      type: "line",
      stacked: true,
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      width: [1, 1, 4],
    },
    title: {
      text: "Total durée d'arret par type d'atlier",
      align: "center",
    },
    xaxis: {
      categories: [
        "Assembly",
        "electrical control ",
        "Cup ",
        "Crimping tool",
        "Preparation",
      ],
    },
    yaxis: [
        {
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: "#008FFB",
          },
          labels: {
            style: {
              colors: "#008FFB",
            },
          },
      }
      ],
  
    tooltip: {
      fixed: {
        enabled: true,
        position: "topLeft", // topRight, topLeft, bottomRight, bottomLeft
        offsetY: 30,
        offsetX: 60,
      },
    },
    legend: {
      horizontalAlign: "left",
      offsetX: 40,
    },
  };

  return (
    <div >
      <ApexChart options={options} series={series} type="line" height={350} />
    </div>
  );
};

const MtbfcolumnChart = () => {
  const series = [
    {
      name: "Downtime ",
      // data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
      data:[86,84,76,70,85,72,55,60,93,74,86,94]
    },
  
  ];
  const options = {
    chart: {
      type: "bar",
      height: 350,
      stacked: true,
      toolbar: {
        show: true,
      },
      zoom: {
        enabled: true,
      },
    },
    title: {
        text: 'Sommes  des arrets par mois',
        align: 'left'
      },
      subtitle: {
        text: '',
        align: 'left'
      },
  
    responsive: [
      {
        breakpoint: 480,
        options: {
          legend: {
            position: "bottom",
            offsetX: -10,
            offsetY: 0,
          },
        },
      },
    ],
    plotOptions: {
      bar: {
        horizontal: false,
        borderRadius: 10,
        dataLabels: {
          total: {
            enabled: true,
            style: {
              fontSize: "13px",
              fontWeight: 900,
            },
          },
        },
      },
    },
    yaxis: {
      title: {
        text: "Somme de Durée des arrets",
      },
    },

    xaxis: {
      categories: [
        "January","February","March","April","May","June","July",
        "August","September","October","November","December"
      ],
      title: {
        text: "",
      },
    },
    legend: {
      position: "right",
      offsetY: 40,
    },
    fill: {
      opacity: 1,
    },
  };

  return (
    <div>
      <ApexChart options={options} series={series} type="bar" height={350} />
    </div>
  );
};
const LineMTBF = ({series}) => {
  // const series = [
  //   {
  //     name: "MTBF ",
  //     // data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
  //     data:[4.1,5.8,5.8,7.5,5.8,6.5,7.8,1.7,5.7,7.1,5.6,5.7]
  //   },
  
  // ];
  const options = {
    chart: {
      type: "line",
      height: 350,
      stacked: true,
      toolbar: {
        show: true,
      },
      zoom: {
        enabled: true,
      },
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          legend: {
            position: "bottom",
            offsetX: -10,
            offsetY: 0,
          },
        },
      },
    ],
    plotOptions: {
      bar: {
        horizontal: false,
        borderRadius: 10,
        dataLabels: {
          total: {
            enabled: true,
            style: {
              fontSize: "13px",
              fontWeight: 900,
            },
          },
        },
      },
    },
    yaxis: {
      title: {
        text: "Somme de MTTR",
      },
    },

    xaxis: {
      categories: [
        "January","February","March","April","May","June","July",
        "August","September","October","November","December"
      ],
      title: {
        
      },
    },
    legend: {
      position: "right",
      offsetY: 40,
    },
    fill: {
      opacity: 1,
    },
  };

  return (
    <div>
      <ApexChart options={options} series={series} type="line" height={350} />
    </div>
  );
};
const AreaChart = () => {
  const series = [
    {
      name: "Downtime ",
      data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
      // data:[86,84,76,70,85,72,55,60,93,74,86,94]
    },
  
  ];
  const options = {
    chart: {
      type: 'area',
      height: 350,
      zoom: {
        enabled: false
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'straight'
    },
    
    title: {
      text: 'Somme des arrets par mois',
      align: 'left'
    },
    // subtitle: {
    //   text: 'Price Movements',
    //   align: 'left'
    // },
    xaxis: {
      categories: [
        "January","February","March","April","May","June","July",
        "August","September","October","November","December"
      ],

    },
    yaxis: {
      opposite: true
    },
    legend: {
      horizontalAlign: 'left'
    }
  }

  

  return (
    <div >
      <ApexChart options={options} series={series} type="area" height={350} />
    </div>
  );
};
const AvatarCard = ({ Name, Rating }) => {
  return (
    <>
      <div class="flex items-center hover:scale-110  transition-all rounded-md cursor-pointer bg-white shadow-2xl p-3">
        <div class="relative mr-4">
          <img
            class="w-14 h-14 rounded"
            src="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
            alt=""
          />
          <span class="absolute bottom-0 right-0 transform translate-y-1/4 w-6 h-6 flex items-center justify-center bg-secondarycolor border-2 border-white dark:border-gray-800 rounded-full">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-6 h-6 font-bold text-xl text-white"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M4.5 12.75l6 6 9-13.5"
              />
            </svg>
          </span>
        </div>{" "}
        <div class="font-normal dark:text-white">
          <div>Jese Leos</div>
          <div class="text-sm text-gray-500 dark:text-gray-400">
            Joined in August 2014
          </div>
          <div class="flex items-center">
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-yellow-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>First star</title>
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
            </svg>
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-yellow-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Second star</title>
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
            </svg>
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-yellow-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Third star</title>
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
            </svg>
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-yellow-400"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Fourth star</title>
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
            </svg>
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-gray-300 dark:text-gray-500"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <title>Fifth star</title>
              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
            </svg>
          </div>
        </div>
      </div>
    </>
  );
};

const AvatarList = () => {
  return (
    <div class="grid  mt-8 grid-cols-4 gap-8">
      <AvatarCard />
      <AvatarCard />
      <AvatarCard />
      <AvatarCard />
   
    </div>
  );
};
export const Statistic = () => {
  return (
    <div className="px-2 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
      <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
        <div>
      
        </div>
        <h2 className="max-w-xl mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto">
          <span className="relative inline-block">
            <svg
              viewBox="0 0 52 24"
              fill="currentColor"
              className="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-blue-gray-100 lg:w-32 lg:-ml-28 lg:-mt-10 sm:block"
            >
              <defs>
                <pattern
                  id="d5589eeb-3fca-4f01-ac3e-983224745704"
                  x="0"
                  y="0"
                  width=".135"
                  height=".30"
                >
                  <circle cx="1" cy="1" r=".7" />
                </pattern>
              </defs>
              <rect
                fill="url(#d5589eeb-3fca-4f01-ac3e-983224745704)"
                width="52"
                height="24"
              />
            </svg>
            <span className="relative">The</span>
          </span>{' '}
          Temps moyen jusqu'a la reparation(MTTR)
        </h2>
        
      </div>
      <div className="relative w-full p-px mx-auto mb-4 overflow-hidden transition-shadow duration-300 border rounded lg:mb-8 lg:max-w-4xl group hover:shadow-xl">
        <div className="absolute bottom-0 left-0 w-full h-1 duration-300 origin-left transform scale-x-0 bg-deep-purple-accent-400 group-hover:scale-x-100" />
        <div className="absolute bottom-0 left-0 w-1 h-full duration-300 origin-bottom transform scale-y-0 bg-deep-purple-accent-400 group-hover:scale-y-100" />
        <div className="absolute top-0 left-0 w-full h-1 duration-300 origin-right transform scale-x-0 bg-deep-purple-accent-400 group-hover:scale-x-100" />
        <div className="absolute bottom-0 right-0 w-1 h-full duration-300 origin-top transform scale-y-0 bg-deep-purple-accent-400 group-hover:scale-y-100" />
        <div className="relative flex flex-col  justify-center items-center h-full py-10 duration-300 bg-white rounded-sm transition-color sm:items-stretch sm:flex-row">
       
          <div className="px-12 py-8 text-center">
            <h6 className="text-4xl font-bold text-deep-purple-accent-400 sm:text-5xl">
             136.80
            </h6>
            <p className="text-center md:text-base">
              Somme de MTTR
            </p>
          </div>

        </div>
      </div>
  
    </div>
  );
};
const index = () => {
  return (
    <div style={{height:'100vh'}} className="h-full">
      <Statistic/>
      <div class="grid  mt-8 grid-cols-12 gap-8">
      {/* <div className="col-span-12">
      <MtbfcolumnChart/>

      </div>
      <div className="col-span-6">
      <AreaChart/>


      </div>
     */}
      <div className="col-span-12">
      <LineMTBF series={[
    {
      name: "MTTR ",
      // data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
      data:[9,11,14,12,11,14,11,10,12,10,14,9]
    },
  
  ]}/>
      </div>
      </div>
{/* <AvatarList/> */}


      {/* <ColumnChart />
      <PieChart />
      <CombinedCharts />
      <CombinedChart/>
      <MtbfcolumnChart/>
      <AreaChart/>
      <LineMTBF series={[
    {
      name: "MTBF ",
      // data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
      data:[4.1,5.8,5.8,7.5,5.8,6.5,7.8,1.7,5.7,7.1,5.6,5.7]
    },
  
  ]}/>
    <LineMTBF series={[
    {
      name: "MTTR ",
      // data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
      data:[9,11,14,12,11,14,11,10,12,10,14,9]
    },
  
  ]}/>
      <LineMTBF series={[
    {
      name: "Disponibilité ",
      // data: [12.40, 14.87, 17.32, 14.27,15.20,16.7,10.38,9.78,18.72,12.78,20.03,14.82],
      data:[0.3234,0.3542,0.2969,0.3787,0.3526,0.3100,0.4081,0.1483,0.3220,0.4055,0.2871,0.3767]
    },
  
  ]}/> */}
    </div>
  );
};
index.Layout = Applayout;

export default index;
