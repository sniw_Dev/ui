import Applayout from "@/layout/Applayout"
import { useEffect } from "react"
import CardService from "@/components/cards/cardservice"
const Search = () => {

  return (
    <div class="grid grid-cols-4 gap-2 "> 
    {[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15].map((element)=>(
      <CardService key={element}/>
    ))}
  
    </div>

  )
}
Search.Layout =Applayout

export default Search