import React from "react";
import Applayout from "@/layout/Applayout";
import dynamic from "next/dynamic";
import Link from "next/link";
// import Hero from "@/components/hero";
import { useRouter } from "next/router";

const Hero = dynamic(() => import("@/components/hero"), {
  ssr: false,
});

const index = () => {
  const CategorieCard = ({ Pathimage, Title }) => {
    return (
      <div className="items-stretch hover:scale-110 text-specialcolor hover:text-secondarycolor  transition-all  cursor-pointer  ">
        <div className="bg-gray-50" style={{ height: "220px" }}>
          <img
            class="h-2/3  hover:border-secondarycolor border  border-primarycolor w-full max-w-full rounded-lg object-cover"
            src={Pathimage}
            alt=""
          />
          <div className="text-sm font-bold text-center mt-1 ">{Title}</div>
        </div>
      </div>
    );
  };

  const CategorieList = () => {
    return (
      <>
        <div class="grid grid-cols-4 gap-4 relative mt-10">
          <Link href="/app/mttr">
            <CategorieCard
              Pathimage={"https://pbs.twimg.com/media/EH_TQ8FU4AIEC4a.png"}
              Title={"MTTR"}
            />
          </Link>
          <Link href="/app/mtbf">
            <CategorieCard
              Pathimage={
                "https://cdn.cui.com/products/image/getimage/9330?typecode=m"
              }
              Title={"MTBF"}
            />
          </Link>
          <Link href="/app/disponibilite">
            <CategorieCard
              Pathimage={
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFYxmFBWOLwLLH6Szv8LckZA-BXvkLWXLByw&usqp=CAU"
              }
              Title={"Disponibilité"}
            />
          </Link>
          <Link href="/app/consomation">
            <CategorieCard
              Pathimage={
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVrpN4migXL2KO6SePRtiV4kLUXHZK1uG70A&usqp=CAU"
              }
              Title={"Cout piéces de rechange"}
            />
          </Link>
        </div>
      </>
    );
  };

  const AvatarCard = ({ Name, Rating }) => {
    return (
      <>
        <div class="flex items-center hover:scale-110  transition-all rounded-md cursor-pointer bg-white shadow-2xl p-3">
          <div class="relative mr-4">
            <img
              class="w-14 h-14 rounded"
              src="https://flowbite.com/docs/images/people/profile-picture-5.jpg"
              alt=""
            />
            <span class="absolute bottom-0 right-0 transform translate-y-1/4 w-6 h-6 flex items-center justify-center bg-secondarycolor border-2 border-white dark:border-gray-800 rounded-full">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6 font-bold text-xl text-white"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M4.5 12.75l6 6 9-13.5"
                />
              </svg>
            </span>
          </div>{" "}
          <div class="font-normal dark:text-white">
            <div>Jese Leos</div>
            <div class="text-sm text-gray-500 dark:text-gray-400">
              Joined in August 2014
            </div>
            <div class="flex items-center">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-yellow-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>First star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-yellow-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Second star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-yellow-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Third star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-yellow-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Fourth star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-300 dark:text-gray-500"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Fifth star</title>
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
              </svg>
            </div>
          </div>
        </div>
      </>
    );
  };

  const AvatarList = () => {
    return (
      <div class="grid  mt-8 grid-cols-4 gap-8">
        <AvatarCard />
        <AvatarCard />
        <AvatarCard />
        <AvatarCard />
      </div>
    );
  };

  const ServiceCard = () => {
    const router = useRouter();

    return (
      <article class="mb-0 break-inside p-4 hover:scale-110  transition-all rounded-xl shadow-2xl bg-white dark:bg-slate-800 flex flex-col bg-clip-border">
        <div class="flex pb-1 items-center justify-between">
          <div class="flex">
            <a class="inline-block mr-4" href="#">
              <img
                class="rounded-full max-w-none w-14 h-14"
                src="https://randomuser.me/api/portraits/men/32.jpg"
              />
            </a>
            <div class="flex flex-col">
              <div class="flex items-center">
                <a class="inline-block text-lg font-bold mr-2" href="#">
                  Savannah Nguyen
                </a>
              </div>
              <div class="text-slate-500 dark:text-slate-300">
                January 22, 2021
              </div>
              <div class="flex items-center">
                <svg
                  aria-hidden="true"
                  class="w-5 h-5 text-yellow-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>First star</title>
                  <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                </svg>
                <svg
                  aria-hidden="true"
                  class="w-5 h-5 text-yellow-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Second star</title>
                  <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                </svg>
                <svg
                  aria-hidden="true"
                  class="w-5 h-5 text-yellow-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Third star</title>
                  <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                </svg>
                <svg
                  aria-hidden="true"
                  class="w-5 h-5 text-yellow-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Fourth star</title>
                  <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                </svg>
                <svg
                  aria-hidden="true"
                  class="w-5 h-5 text-gray-300 dark:text-gray-500"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Fifth star</title>
                  <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                </svg>
              </div>
            </div>
          </div>
        </div>
        <h2 class="text-lg  font-extrabold">Web Design templates Selection</h2>
        <div class="py-1 font-normal text-specialcolor">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</p>
        </div>
        <button
          onClick={() => router.push("/app/profile")}
          type="submit"
          class="text-white mr-2 bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Consulter
        </button>
      </article>
    );
  };

  const ServiceList = () => {
    return (
      <div class="grid  mt-8 grid-cols-3 gap-6">
        <ServiceCard />
        <ServiceCard />
        <ServiceCard />
        <ServiceCard />
        <ServiceCard />

        <ServiceCard />
        <ServiceCard />
        <ServiceCard />
        <ServiceCard />
      </div>
    );
  };

  const TopServices = () => {
    return (
      <div>
        <div className="flex justify-between items-center">
          <div>
            <div className="text-sm text-secondarycolor text-left">
              Top Intervenants
            </div>
            <div className="text-3xl text-primarycolor">
              Most Active Intervenants
            </div>
          </div>
          <div>
            <span className="text-lg text-primarycolor  font-bold cursor-pointer hover:text-secondarycolor">
              See All{" "}
            </span>
          </div>
        </div>
        <AvatarList />
      </div>
    );
  };

  const RecomandedServices = () => {
    return (
      <div>
        <div className="flex justify-between items-center mt-16">
          <div>
            <div className="text-sm text-secondarycolor text-left">
              Recomanded services
            </div>
            <div className="text-3xl text-primarycolor">
              Most popular services
            </div>
          </div>
          <div>
            <span className="text-lg text-primarycolor  font-bold cursor-pointer hover:text-secondarycolor">
              See All{" "}
            </span>
          </div>
        </div>
        <ServiceList />
      </div>
    );
  };
  return (
    <div>
      <Hero />
      <div className="w-full flex justify-center">
        <CategorieList />
      </div>

      {/* <TopServices />
      <RecomandedServices /> */}
    </div>
  );
};
index.Layout = Applayout;

export default index;
