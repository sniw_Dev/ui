import { useState, useEffect } from "react";
import styles from "@/styles/welcome.module.css";
import { useRouter } from 'next/navigation';
const Welcome = () => {
  const [ActiveStep, setActiveStep] = useState(0);
  const [AccountType, setAccountType] = useState(0);
  const router = useRouter();

  const AccountTypePage = () => {
    const UpdatePageForm = (AccountType) => {
      setAccountType(AccountType);
      setActiveStep(1);
    };
    return (
      <div className="flex-col justify-center items-center p-4">
        <div className="text-center p-4 w-full ">
          <div className="text-3xl">Complete Your Account Information !</div>
          <div className="text-center px-30 py-2 font-mediums text-specialcolor ">
            Thank you for signing up! Please take a moment to complete your
            account information so we can provide you with the best possible
            service.
          </div>
        </div>
        <div className="text-center  px-30 w-full ">
          <div className="text-center font-mediums text-primarycolor  ">
            <div className="text-2xl mb-3">Select Your account type : </div>

            <ul class="grid w-full gap-6 md:grid-cols-2">
              <li
                onClick={() => {
                  UpdatePageForm(1);
                }}
              >
                <input
                  type="checkbox"
                  id="react-option"
                  value=""
                  class="hidden peer"
                  required=""
                />
                <label
                  for="react-option"
                  class=" flex items-center justify-center w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-secondarycolor hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700"
                >
                  <div class="flex-col items-center justify-center">
                    <div>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        class="w-7 h-7 mb-2 text-secondarycolor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
                        />
                      </svg>
                    </div>
                    <div class="w-full text-lg font-semibold">Client</div>
                  </div>
                </label>
              </li>
              <li
                onClick={() => {
                  UpdatePageForm(2);
                }}
              >
                <input
                  type="checkbox"
                  id="flowbite-option"
                  value=""
                  class="hidden peer"
                />
                <label
                  for="flowbite-option"
                  class=" flex w-1/1 items-center justify-center w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-secondarycolor hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700"
                >
                  <div class="flex-col items-center justify-center">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="mb-2 text-green-400 w-7 h-7"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M2.25 18.75a60.07 60.07 0 0115.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 013 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 00-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 01-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 003 15h-.75M15 10.5a3 3 0 11-6 0 3 3 0 016 0zm3 0h.008v.008H18V10.5zm-12 0h.008v.008H6V10.5z"
                      />
                    </svg>

                    <div class="w-full text-lg font-semibold">Intervenant</div>
                  </div>
                </label>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  };

  const IntervenantPage = () => {
    const [IntervenantSteps, setIntervenantSteps] = useState(0);

    const StepperHeader = () => {
      return (
        <ol class="flex items-center justify-center pl-0 w-full mb-4 sm:mb-5">
          <li class="flex w-full items-center text-pink-500 dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-blue-800">
            <div
              className={`flex items-center justify-center w-10 h-10 ${
                IntervenantSteps == 0 ? "bg-secondarycolor" : "bg-primarycolor"
              } rounded-full lg:h-12 lg:w-12 dark:bg-blue-800 shrink-0`}
            >
              <svg
                aria-hidden="true"
                class="w-6 h-6 text-white lg:w-6 lg:h-6 dark:text-blue-300"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M10 2a1 1 0 00-1 1v1a1 1 0 002 0V3a1 1 0 00-1-1zM4 4h3a3 3 0 006 0h3a2 2 0 012 2v9a2 2 0 01-2 2H4a2 2 0 01-2-2V6a2 2 0 012-2zm2.5 7a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm2.45 4a2.5 2.5 0 10-4.9 0h4.9zM12 9a1 1 0 100 2h3a1 1 0 100-2h-3zm-1 4a1 1 0 011-1h2a1 1 0 110 2h-2a1 1 0 01-1-1z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </li>
          <li class="flex w-full items-center text-pink-500 dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-blue-800">
            <div
              className={`flex items-center justify-center w-10 h-10 ${
                IntervenantSteps == 1 ? "bg-secondarycolor" : "bg-primarycolor"
              } rounded-full lg:h-12 lg:w-12 dark:bg-blue-800 shrink-0`}
            >
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-white lg:w-6 lg:h-6 dark:text-gray-100"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M4 4a2 2 0 00-2 2v1h16V6a2 2 0 00-2-2H4z"></path>
                <path
                  fill-rule="evenodd"
                  d="M18 9H2v5a2 2 0 002 2h12a2 2 0 002-2V9zM4 13a1 1 0 011-1h1a1 1 0 110 2H5a1 1 0 01-1-1zm5-1a1 1 0 100 2h1a1 1 0 100-2H9z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </li>

          <li class="flex  items-center text-pink-500 dark:text-blue-500  dark:after:border-blue-800">
            <div
              className={`flex items-center justify-center w-10 h-10 ${
                IntervenantSteps == 2 ? "bg-secondarycolor" : "bg-primarycolor"
              } rounded-full lg:h-12 lg:w-12 dark:bg-blue-800 shrink-0`}
            >
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-white lg:w-6 lg:h-6 dark:text-gray-100"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path>
                <path
                  fill-rule="evenodd"
                  d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm9.707 5.707a1 1 0 00-1.414-1.414L9 12.586l-1.293-1.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </li>
        </ol>
      );
    };

    const DocumentUploadForm = () => {
      return (
        <>
          <div className="mt-3 text-3xl">
            Account verification
            <p className="mt-2 font-light text-sm">
              Please upload scanned copies of your CIN, patent, or RNE files
              below. These documents are necessary for verification purposes and
              to comply with legal and regulatory requirements. Please make sure
              that the files are in a clear and readable format, preferably in
              PDF or JPEG format.
            </p>
          </div>
          <div class="grid grid-cols-5 gap-4">
            <div>
              <img
                class="h-auto max-w-full rounded-lg"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII="
                alt=""
              />
            </div>
            <div>
              <img
                class="h-auto max-w-full rounded-lg"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII="
                alt=""
              />
            </div>
            <div>
              <img
                class="h-auto max-w-full rounded-lg"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII="
                alt=""
              />
            </div>
            <div>
              <img
                class="h-auto max-w-full rounded-lg"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII="
                alt=""
              />
            </div>
            <div>
              <img
                class="h-auto max-w-full rounded-lg"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII="
                alt=""
              />
            </div>
          </div>
          <div class="flex items-center justify-center w-full mt-3">
            <label
              for="dropzone-file"
              class="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600"
            >
              <div class="flex flex-col items-center justify-center pt-5 pb-6">
                <svg
                  aria-hidden="true"
                  class="w-10 h-10 mb-3 text-gray-400"
                  fill="none"
                  stroke="currentColor"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                  ></path>
                </svg>
                <p class="mb-2 text-sm text-gray-500 dark:text-gray-400">
                  <span class="font-semibold">Click to upload</span> or drag and
                  drop
                </p>
                <p class="text-xs text-gray-500 dark:text-gray-400">
                  SVG, PNG, JPG or GIF (MAX. 800x400px)
                </p>
              </div>
              <input id="dropzone-file" type="file" class="hidden" />
            </label>
          </div>
          <div className="w-full flex justify-end mt-3">
            <button
              onClick={() => {
                setIntervenantSteps(IntervenantSteps - 1);
              }}
              type="submit"
              class="text-white mr-2 bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Previous
            </button>
            <button
              onClick={() => {
                setIntervenantSteps(IntervenantSteps + 1);
              }}
              type="submit"
              class="text-white bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Next
            </button>
          </div>
        </>
      );
    };
    const PersonnalInformation = () => {
      return (
        <div className="mt-3">
          <div className="mt-3 text-3xl">
            Personal Information
            <p className="mt-2 font-light text-sm">
              Please fill out the fields below with your personal information.
              We require this information in order to provide you with our
              services and to comply with legal and regulatory requirements.
            </p>
          </div>
          <form>
            <div class="grid md:grid-cols-2 md:gap-6">
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  Address
                </label>
                <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="6 Rue marseille el mourouj "
                  required
                />
              </div>
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  Phone
                </label>
                <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="+216 21 345 678"
                  required
                />
              </div>
            </div>
            <div class="grid md:grid-cols-2 md:gap-6">
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  State
                </label>
                <select
                  id="countries"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                >
                  <option>Ben Arous</option>
                  <option>Ariena</option>
                  <option>Tunis</option>
                  <option>Manouba</option>
                </select>
              </div>
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  Cin
                </label>
                <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="12345678"
                  required
                />
              </div>
            </div>
            <div className="mb-2">
              <label
                for="message"
                class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
              >
                Bio
              </label>
              <textarea
                id="message"
                rows="4"
                class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Write something about yourself..."
              ></textarea>
            </div>
            <div className="w-full flex justify-end">
              <button
                onClick={() => {
                  setIntervenantSteps(IntervenantSteps + 1);
                }}
                type="submit"
                class="text-white bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Next
              </button>
            </div>
          </form>
        </div>
      );
    };

    const VerificationForm = () => {
      return (
        <div className="">
          <div class="max-w-sm mx-auto md:max-w-lg mb-5">
            <div class="w-full">
              <div class="bg-white h-64 py-3 rounded text-center">
                <div className="mt-3 text-3xl">
                  Verify your email address
                  <p className="mt-2 font-light text-sm">
                    <span>
                      We emailed you a six-digit code to *******@company.com.
                    </span>
                    <span>
                      Enter the code below to confirm your email address.
                    </span>
                  </p>
                </div>
                <div
                  id="otp"
                  class="flex flex-row justify-center text-center  mt-2"
                >
                  <input
                    class="m-2 border-secondarycolor h-20 w-20 text-center ring-0 border-secondarycolor focus:border-secondarycolor focus:ring-secondarycolor form-control rounded"
                    type="text"
                    id="first"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="second"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="third"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="fourth"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="fifth"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="sixth"
                    maxLength="1"
                  />
                </div>

                <div class="flex justify-center text-lg text-center mt-3">
                  <a class="flex items-center  text-primarycolor hover:text-secondarycolor cursor-pointer">
                    <span class="">Resend Code</span>
                    <i class="bx bx-caret-right ml-1"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full flex justify-end mt-3">
            <button
              onClick={() => {
                setIntervenantSteps(IntervenantSteps - 1);
              }}
              type="submit"
              class="text-white mr-2 bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Previous
            </button>
            <button
              onClick={() => {
                setIntervenantSteps(IntervenantSteps + 1);
              }}
              type="submit"
              class="text-white bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Finish
            </button>
          </div>
        </div>
      );
    };

    const SuccessPage = () => {
      setTimeout(()=>{
        router.push('/app')

    },3000)
      return (
        <div className="p-24">
          <div className="text-3xl text-green-400 text-center w-full flex justify-center items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-48 h-48 "
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z"
              />
            </svg>
          </div>
          <div className="text-3xl text-primarycolor text-center">
            Email Verified Successefully
          </div>
          <div className=" text-specialcolor text-center">
            Redirection to the home page...
          </div>
        </div>
      );
    };
    return (
      <>
        <StepperHeader />
        <div>
          {IntervenantSteps == 0 && <PersonnalInformation />}
          {IntervenantSteps == 1 && <DocumentUploadForm />}
          {IntervenantSteps == 2 && <VerificationForm />}
          {IntervenantSteps == 3 && <SuccessPage />}
        </div>
      </>
    );
  };

  const ClientPage = () => {
    const [ClientSteps, setClientSteps] = useState(0);
    const ClientStepperHeader = () => {
      return (
        <ol class="flex items-center justify-center pl-0 w-full mb-4 sm:mb-5">
          <li class="flex w-full items-center text-pink-500 dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-blue-800">
            <div
              className={`flex items-center justify-center w-10 h-10 ${
                ClientSteps == 0 ? "bg-secondarycolor" : "bg-primarycolor"
              } rounded-full lg:h-12 lg:w-12 dark:bg-blue-800 shrink-0`}
            >
              <svg
                aria-hidden="true"
                class="w-6 h-6 text-white lg:w-6 lg:h-6 dark:text-blue-300"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M10 2a1 1 0 00-1 1v1a1 1 0 002 0V3a1 1 0 00-1-1zM4 4h3a3 3 0 006 0h3a2 2 0 012 2v9a2 2 0 01-2 2H4a2 2 0 01-2-2V6a2 2 0 012-2zm2.5 7a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm2.45 4a2.5 2.5 0 10-4.9 0h4.9zM12 9a1 1 0 100 2h3a1 1 0 100-2h-3zm-1 4a1 1 0 011-1h2a1 1 0 110 2h-2a1 1 0 01-1-1z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </li>
          <li class="flex w-full items-center text-pink-500 dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block dark:after:border-blue-800">
            <div
              className={`flex items-center justify-center w-10 h-10 ${
                ClientSteps == 1 ? "bg-secondarycolor" : "bg-primarycolor"
              } rounded-full lg:h-12 lg:w-12 dark:bg-blue-800 shrink-0`}
            >
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-white lg:w-6 lg:h-6 dark:text-gray-100"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M4 4a2 2 0 00-2 2v1h16V6a2 2 0 00-2-2H4z"></path>
                <path
                  fill-rule="evenodd"
                  d="M18 9H2v5a2 2 0 002 2h12a2 2 0 002-2V9zM4 13a1 1 0 011-1h1a1 1 0 110 2H5a1 1 0 01-1-1zm5-1a1 1 0 100 2h1a1 1 0 100-2H9z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </li>

          <li class="flex  items-center text-pink-500 dark:text-blue-500  dark:after:border-blue-800">
            <div
              className={`flex items-center justify-center w-10 h-10 ${
                ClientSteps == 2 ? "bg-secondarycolor" : "bg-primarycolor"
              } rounded-full lg:h-12 lg:w-12 dark:bg-blue-800 shrink-0`}
            >
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-white lg:w-6 lg:h-6 dark:text-gray-100"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path>
                <path
                  fill-rule="evenodd"
                  d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm9.707 5.707a1 1 0 00-1.414-1.414L9 12.586l-1.293-1.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </li>
        </ol>
      );
    };
    const PersonnalInformation = () => {
      return (
        <div className="mt-3">
          <div className="mt-3 text-3xl">
            Personal Information
            <p className="mt-2 font-light text-sm">
              Please fill out the fields below with your personal information.
              We require this information in order to provide you with our
              services and to comply with legal and regulatory requirements.
            </p>
          </div>
          <form>
            <div class="grid md:grid-cols-2 md:gap-6">
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  Address
                </label>
                <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="6 Rue marseille el mourouj "
                  required
                />
              </div>
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  Phone
                </label>
                <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="+216 21 345 678"
                  required
                />
              </div>
            </div>
            <div class="grid md:grid-cols-2 md:gap-6">
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  State
                </label>
                <select
                  id="countries"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                >
                  <option>Ben Arous</option>
                  <option>Ariena</option>
                  <option>Tunis</option>
                  <option>Manouba</option>
                </select>
              </div>
              <div class="relative z-0 w-full mb-6 group">
                <label
                  for="email"
                  class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
                >
                  Cin
                </label>
                <input
                  type="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="12345678"
                  required
                />
              </div>
            </div>
            <div className="mb-2">
              <label
                for="message"
                class="block mb-2 text-sm font-medium text-specialcolor dark:text-white"
              >
                Bio
              </label>
              <textarea
                id="message"
                rows="4"
                class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Write something about yourself..."
              ></textarea>
            </div>
            <div className="w-full flex justify-end">
              <button
                onClick={() => {
                  setClientSteps(ClientSteps + 1);
                }}
                type="submit"
                class="text-white bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Next
              </button>
            </div>
          </form>
        </div>
      );
    };

    const VerificationForm = () => {
      return (
        <div className="">
          <div class="max-w-sm mx-auto md:max-w-lg mb-5">
            <div class="w-full">
              <div class="bg-white h-64 py-3 rounded text-center">
                <div className="mt-3 text-3xl">
                  Verify your email address
                  <p className="mt-2 font-light text-sm">
                    <span>
                      We emailed you a six-digit code to *******@company.com.
                    </span>
                    <span>
                      Enter the code below to confirm your email address.
                    </span>
                  </p>
                </div>
                <div
                  id="otp"
                  class="flex flex-row justify-center text-center  mt-2"
                >
                  <input
                    class="m-2 border-secondarycolor h-20 w-20 text-center ring-0 border-secondarycolor focus:border-secondarycolor focus:ring-secondarycolor form-control rounded"
                    type="text"
                    id="first"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="second"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="third"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="fourth"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="fifth"
                    maxLength="1"
                  />
                  <input
                    class="m-2 border h-20 w-20 text-center form-control rounded"
                    type="text"
                    id="sixth"
                    maxLength="1"
                  />
                </div>

                <div class="flex justify-center text-lg text-center mt-3">
                  <a class="flex items-center  text-primarycolor hover:text-secondarycolor cursor-pointer">
                    <span class="">Resend Code</span>
                    <i class="bx bx-caret-right ml-1"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full flex justify-end mt-3">
            <button
              onClick={() => {
                setClientSteps(ClientSteps - 1);
              }}
              type="submit"
              class="text-white mr-2 bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Previous
            </button>
            <button
              onClick={() => {
                setClientSteps(ClientSteps + 1);
              }}
              type="submit"
              class="text-white bg-primarycolor hover:bg-secondarycolor focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Finish
            </button>
          </div>
        </div>
      );
    };

    const SuccessPage = () => {
      setTimeout(()=>{
        router.push('/app')

    },3000)
      return (
        <div className="p-24">
          <div className="text-3xl text-green-400 text-center w-full flex justify-center items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke-width="1.5"
              stroke="currentColor"
              class="w-48 h-48 "
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z"
              />
            </svg>
          </div>
          <div className="text-3xl text-primarycolor text-center">
            Email Verified Successefully
          </div>
          <div className=" text-specialcolor text-center">
            Redirection to the home page...
          </div>
        </div>
      );
    };
    return (
      <>
        <ClientStepperHeader />
        <div>
          {ClientSteps == 0 && <PersonnalInformation />}
          {ClientSteps == 1 && <VerificationForm />}
          {ClientSteps == 2 && <SuccessPage />}
        </div>
      </>
    );
  };

  return (
    <div className={styles.main}>
      <div
        style={{ width: "50vw" }}
        className="bg-white p-5 rounded-lg  justify-center flex-col items-center "
      >
        {ActiveStep == 0 && <AccountTypePage />}

        {ActiveStep == 1 && AccountType == 2 && <IntervenantPage />}

        {ActiveStep == 1 && AccountType == 1 && <ClientPage />}

    
      </div>
    </div>
  );
};

export default Welcome;
