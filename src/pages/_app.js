import '@/styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';




export default function App({ Component, pageProps }) {
  const Layout = Component.Layout || EmptyLayout
  return <Layout><Component {...pageProps} /></Layout>
}

const EmptyLayout =({children})=><>{children}</>;